----------------------------------------------------------------------------------
-- Company: UTN - FRP
-- Engineer: Maggiolo Gustavo
-- 
-- Create Date:    20:09:55 04/01/2022 
-- Design Name:    Intro_VHDL-TranfRegistros
-- Module Name:    EvalCitas - Behavioral 
-- Project Name: 	 Evaluador de Citas
-- Target Devices: 
-- Tool versions: 
-- Description: Evaluador de citas, desarrollado en la clase de teoria.
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity EvaluadorCitas is
	port (
			entradas: in  std_logic_vector(2 downto 0);
			salida: out std_logic );
	end EvaluadorCitas;
	
architecture TransfRegistros of EvaluadorCitas is
	
begin
	process(entradas)
	begin
	case entradas is
		 when "000" => salida <= '0';
		 when "001" => salida <= '0';
		 when "010" => salida <= '0';
		 when "011" => salida <= '1';
		 when "100" => salida <= '0';
		 when "101" => salida <= '1';
		 when "110" => salida <= '1';
		 when "111" => salida <= '1';
		 when others => salida <= '0';
	end case;
	end process;
end TransfRegistros;

