----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    20:49:14 09/02/2022 
-- Design Name: 
-- Module Name:    FlipFlip_D - Behavioral 
-- Project Name: 
-- Target Devices: BASYS 2
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity FlipFlip_D is
    Port ( mclk : in  STD_LOGIC;
           data_in : in  STD_LOGIC;
           rst : in  STD_LOGIC;
           q_out : out  STD_LOGIC);
end FlipFlip_D;

architecture Behavioral of FlipFlip_D is

begin
	process(mclk, rst)
	begin
		if(rst = '1') then 
			q_out <= '0';
		elsif(rising_edge(mclk)) then
			q_out <= data_in;
		end if;
	end process;
end Behavioral;

