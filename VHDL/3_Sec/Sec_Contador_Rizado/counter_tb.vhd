--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   04:55:50 09/03/2022
-- Design Name:   
-- Module Name:   /home/ise/ise_projects/1_Clases/3_Secuencial/Sec_Contador_Rizado/counter_tb.vhd
-- Project Name:  Sec_Contador_Rizado
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: counter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY counter_tb IS
END counter_tb;
 
ARCHITECTURE behavior OF counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT counter
    PORT(
         sw_clk : IN  std_logic;
         rst : IN  std_logic;
         q : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sw_clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal q : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant sw_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: counter PORT MAP (
          sw_clk => sw_clk,
          rst => rst,
          q => q
        );

   -- Clock process definitions
   sw_clk_process :process
   begin
		sw_clk <= '0';
		wait for sw_clk_period/2;
		sw_clk <= '1';
		wait for sw_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      --wait for 100 ns;	
		
		rst <= '1';
      
	  wait for sw_clk_period*10;
		
      -- insert stimulus here 
		
		rst <= '0';
      
	  wait;
   end process;

END;
