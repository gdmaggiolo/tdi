----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    01:55:35 09/03/2022 
-- Design Name: 
-- Module Name:    rin_counter - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity rin_counter is
    Port ( sw_clk : in  STD_LOGIC;
           rst 	: in  STD_LOGIC;
           q 		: out  STD_LOGIC_VECTOR (3 downto 0));
end rin_counter;

architecture Behavioral of rin_counter is
	signal	auxq : STD_LOGIC_VECTOR (3 downto 0);
begin
Anillo:
	process (rst,sw_clk) begin
		if rst = '1' then
				auxq(2 downto 0) 	<= (others => '0');
				auxq(3)				<= '1';
		else if rising_edge(sw_clk) then
					auxq <= auxq(0) & auxq(3 downto 1);	--auxq = auxq(0) & auxq(3) & auxq(2) & auxq(1)
				end if;
		end if;
	end process Anillo;
	q <= auxq;
end Behavioral;




--Anillo:
--	process (rst,sw_clk) begin
--		if rst = '1' then
--				auxq <= "1000";
--		else if rising_edge(sw_clk) then
--				auxq(0) <= auxq(1);
--				auxq(1) <= auxq(2);
--				auxq(2) <= auxq(3);
--				auxq(3) <= auxq(0);
--			end if;
--		end if;
--	end process Anillo;
--	q <= auxq;
--end Behavioral;