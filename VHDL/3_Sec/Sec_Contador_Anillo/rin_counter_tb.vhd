--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   03:49:34 09/03/2022
-- Design Name:   
-- Module Name:   /home/ise/ise_projects/1_Clases/3_Secuencial/Sec_Contador_Anillo/rin_counter_tb.vhd
-- Project Name:  Sec_Contador_Anillo
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: rin_counter
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY rin_counter_tb IS
END rin_counter_tb;
 
ARCHITECTURE behavior OF rin_counter_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT rin_counter
    PORT(
         sw_clk : IN  std_logic;
         rst : IN  std_logic;
         q : OUT  std_logic_vector(3 downto 0)
        );
    END COMPONENT;
    

   --Inputs
   signal sw_clk : std_logic := '0';
   signal rst : std_logic := '0';

 	--Outputs
   signal q : std_logic_vector(3 downto 0);

   -- Clock period definitions
   constant sw_clk_period : time := 10 ns;
 
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: rin_counter PORT MAP (
          sw_clk => sw_clk,
          rst => rst,
          q => q
        );

   -- Clock process definitions
   sw_clk_process :process
   begin
		sw_clk <= '0';
		wait for sw_clk_period/2;
		sw_clk <= '1';
		wait for sw_clk_period/2;
   end process;
 

   -- Stimulus process
   stim_proc: process
   begin		
      -- hold reset state for 100 ns.
      --wait for 100 ns;	
		
		rst <= '1';
		
      wait for sw_clk_period*10;

      -- insert stimulus here 
		
		rst <= '0';
		
      wait;
   end process;

END;
