----------------------------------------------------------------------------------
-- Company: UTN - FRP
-- Engineer: Maggiolo Gustavo
-- 
-- Create Date:    20:20:55 03/28/2020 
-- Design Name:    Intro_VHDL-Funcional
-- Module Name:    Mux - Behavioral 
-- Project Name: 	 Intro VHDL
-- Target Devices: 
-- Tool versions: 
-- Description: Mux implementado funcionalmente.
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
	port (
			a: in std_logic;
			b: in std_logic;
			selec: in std_logic;
			salida: out std_logic );
	end mux;
	
architecture funcional of mux is
begin
	process (a,b,selec)
	begin
		if (selec = '0') then
			salida <= a;
		else 
			salida <= b;
		end if;
	end process;
end funcional;

