----------------------------------------------------------------------------------
-- Company: UTN - FRP
-- Engineer: Maggiolo Gustavo
-- 
-- Create Date:    20:20:55 04/04/2020 
-- Design Name:    Intro_VHDL-Estructural
-- Module Name:    Mux - Flujo de Datos 
-- Project Name: 	 Intro VHDL
-- Target Devices: 
-- Tool versions: 
-- Description: Mux implementado con Flujo de datos, es decir,
--					 implementado con las funciones elementales AND,
--					 OR y NOT;
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
	port (
			a: in std_logic;
			b: in std_logic;
			selec: in std_logic;
			salida: out std_logic );
	end mux;
	
architecture flujodatos of mux is

signal ax,bx,nosel: std_logic;

begin
		nosel <= NOT selec;
		ax <= a AND nosel;
		bx <= b AND selec;
		salida <= ax OR bx;
		
end flujodatos;

