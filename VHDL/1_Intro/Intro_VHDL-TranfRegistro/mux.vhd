----------------------------------------------------------------------------------
-- Company: UTN - FRP
-- Engineer: Maggiolo Gustavo
-- 
-- Create Date:    20:20:55 04/04/2020 
-- Design Name:    Intro_VHDL-TranfRegistros
-- Module Name:    Mux - Behavioral 
-- Project Name: 	 Intro VHDL
-- Target Devices: 
-- Tool versions: 
-- Description: Mux implementado comportamentalmente.
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity mux is
	port (
			a: in std_logic;
			b: in std_logic;
			selec: in std_logic;
			salida: out std_logic );
	end mux;
	
architecture TransfRegistros of mux is

begin

	salida <= a WHEN selec = '0' ELSE b;

end TransfRegistros;

