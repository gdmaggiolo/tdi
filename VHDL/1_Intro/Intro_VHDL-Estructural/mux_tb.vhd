--------------------------------------------------------------------------------
-- Company: 
-- Engineer:
--
-- Create Date:   22:33:10 04/04/2020
-- Design Name:   
-- Module Name:   /home/ise/ise_projects/Intro_VHDL-Estructural/mux_tb.vhd
-- Project Name:  Intro_VHDL-Estructural
-- Target Device:  
-- Tool versions:  
-- Description:   
-- 
-- VHDL Test Bench Created by ISE for module: mux
-- 
-- Dependencies:
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
--
-- Notes: 
-- This testbench has been automatically generated using types std_logic and
-- std_logic_vector for the ports of the unit under test.  Xilinx recommends
-- that these types always be used for the top-level I/O of a design in order
-- to guarantee that the testbench will bind correctly to the post-implementation 
-- simulation model.
--------------------------------------------------------------------------------
LIBRARY ieee;
USE ieee.std_logic_1164.ALL;
 
-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--USE ieee.numeric_std.ALL;
 
ENTITY mux_tb IS
END mux_tb;
 
ARCHITECTURE behavior OF mux_tb IS 
 
    -- Component Declaration for the Unit Under Test (UUT)
 
    COMPONENT mux
    PORT(
         a : IN  std_logic;
         b : IN  std_logic;
         selec : IN  std_logic;
         salida : OUT  std_logic
        );
    END COMPONENT;
    
   --Inputs
   signal a : std_logic := '0';
   signal b : std_logic := '0';
   signal selec : std_logic := '0';

 	--Outputs
   signal salida : std_logic;
    
BEGIN
 
	-- Instantiate the Unit Under Test (UUT)
   uut: mux PORT MAP (
          a => a,
          b => b,
          selec => selec,
          salida => salida
        );
   
   -- Stimulus process
   stim_proc: process
   begin		
      -- insert stimulus here 
		a <= '0';
		b <= '0';
		selec <= '0';
		wait for 20 ns;
		
		a <= '1';
		b <= '0';
		selec <= '0';
		wait for 20 ns;
		
		a <= '0';
		b <= '1';
		selec <= '0';
		wait for 20 ns;
		
		a <= '1';
		b <= '1';
		selec <= '0';
		wait for 20 ns;
		
		a <= '0';
		b <= '0';
		selec <= '1';
		wait for 20 ns;
		
		a <= '1';
		b <= '0';
		selec <= '1';
		wait for 20 ns;
		
		a <= '0';
		b <= '1';
		selec <= '1';
		wait for 20 ns;
		
		a <= '1';
		b <= '1';
		selec <= '1';
		wait for 20 ns;
      wait;
   end process;

END;
