----------------------------------------------------------------------------------
-- Company: UTN - FRP
-- Engineer: Maggiolo Gustavo
-- 
-- Create Date:    20:20:55 03/28/2020 
-- Design Name:    Intro_VHDL-Estructural
-- Module Name:    Mux - Behavioral 
-- Project Name: 	 Intro VHDL
-- Target Devices: 
-- Tool versions: 
-- Description: Mux implementado estructuralmente, es decir,
--					 usando componentes ya definidos en el entorno, o bien
--					 otras ENTITY definidas por nosotros.	
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;					--Requerido para usar las primitivas
use UNISIM.VComponents.all;	--Requerido para usar las primitivas

entity mux is
	port (
			a: in std_logic;
			b: in std_logic;
			selec: in std_logic;
			salida: out std_logic );
	end mux;
	
architecture estructural of mux is

component and2
	port(I0,I1: in std_logic; O: out std_logic);
end component;

component or2
	port(I0,I1: in std_logic; O: out std_logic);
end component;

component inv
	port(I: in std_logic; O: out std_logic);
end component;

signal ax,bx,nosel: std_logic;

begin
u0:inv port map(I=>selec,O=>nosel);
u1:and2 port map(I0=>a,I1=>nosel,O=>ax);
u2:and2 port map(I0=>b,I1=>selec,O=>bx);
u3:or2 port map(I0=>ax,I1=>bx,O=>salida);	
end estructural;

